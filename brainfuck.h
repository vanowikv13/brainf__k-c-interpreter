#pragma once

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>

#define SIZE_OF_MEM 1024 * 64
#define LOOP_MAX_NESTED 1000

bool incrementPointer(char **bptr, char *mem)
{
    if (*bptr == &(mem[SIZE_OF_MEM - 1]))
        return false;
    (*bptr)++;
    return true;
}

bool decrementPointer(char **bptr, char *mem)
{
    if (*bptr == mem)
        return false;
    (*bptr)--;
    return true;
}

void incrementPointerValue(char *bptr)
{
    ++*bptr;
}

void decrementPointerValue(char *bptr)
{
    --*bptr;
}

void outputByte(char *bptr)
{
    printf("%d\n", *bptr);
}

void setPointerValue(char *bptr, char value)
{
    *bptr = value;
}

int finishLoop(int i, char *brainfuckCommands)
{
    int k = 1;
    i++;
    while (k != 0 && i < strlen(brainfuckCommands))
    {
        if (brainfuckCommands[i] == '[')
            k++;

        if (brainfuckCommands[i] == ']')
            k--;
        i++;
    }
    if (brainfuckCommands[i] == ']')
        i++;
    return i;
}

char *getBrainfuckInstructionsFromFile(char *fileName)
{
    if (fileName == NULL)
        return "";

    FILE *fptr;

    fptr = fopen(fileName, "r");
    if (fptr == NULL)
    {
        printf("Wrong file name\n");
        exit(1);
    }

    fseek(fptr, 0, SEEK_END);
    size_t fsize = ftell(fptr);
    fseek(fptr, 0, SEEK_SET);

    char *brainfuckCommands = malloc(fsize + 1);
    fread(brainfuckCommands, 1, fsize, fptr);
    fclose(fptr);
    return brainfuckCommands;
}

void brainfuck(char *fileName)
{
    size_t start_loop_i[LOOP_MAX_NESTED];
    size_t start_top = 0;
    //initialize memory and set it's cells values to 0
    char *mem = calloc(0, sizeof(char) * SIZE_OF_MEM);
    char *bptr = mem;

    char *brainfuckCommands = getBrainfuckInstructionsFromFile(fileName);

    bool jmp = false;

    //looping through signs
    size_t i = 0, f = 0;
    while (i < strlen(brainfuckCommands))
    {
        switch (brainfuckCommands[i])
        {

        //all directives like ",.<...etc." will be ignored
        case '"':
            i++;
            while (brainfuckCommands[i] != '"')
                i++;
            i++;

        case '>':
            if (!incrementPointer(&bptr, mem))
            {
                printf("overflow of memory\n");
                exit(1);
            }
            break;
        case '<':
            if (!decrementPointer(&bptr, mem))
            {
                printf("Underflow of memory\n");
                exit(1);
            }
            break;
        case '+':
            incrementPointerValue(bptr);
            break;
        case '-':
            decrementPointerValue(bptr);
            break;
        case '.':
            outputByte(bptr);
            break;
        case ',':
            setPointerValue(bptr, fgetc(stdin));
            break;
        case '[':
            if (*bptr)
            {
                if (!jmp)
                {
                    if (start_top + 1 < LOOP_MAX_NESTED - 1)
                    {
                        start_top++;
                        start_loop_i[start_top] = i;
                    }
                    else
                    {
                        printf("too many nested loops\n");
                        exit(1);
                    }
                }
                jmp = false;
            }
            else
            {
                i = finishLoop(i, brainfuckCommands);
                start_top--;
                jmp = false;
            }
            break;

        case ']':
            i = start_loop_i[start_top] - 1;
            jmp = true;
            break;
        }
        i++;
    }
}