#include "brainfuck.h"

int main(int argc, char **argv)
{
    char c;
    char *fileName = NULL;
    FILE *fptr;

    while ((c = getopt(argc, argv, "o:")) != -1)
    {
        if (c == 'o')
        {
            size_t n = strlen(optarg);
            fileName = malloc(n + 1);
            strncpy(fileName, optarg, n);
        }
    }

    if (fileName != NULL)
    {
        brainfuck(fileName);
    }
    else
    {
        printf("user didn't specify the fileName\n");
        exit(1);
    }
    
    return 0;
}